<?php

use Drupal\bulk_process\ItemProcessor\BulkProcessItemProcessor;

/**
 * Defines hook to provide information about operations.
 */
function hook_bulk_process_operations_info() {
  return [
    [
      // The machine name of the operation. Used to track the type of the
      // change made.
      'name' => '',
      // The entity to apply the change.
      'entity_type' => 'node',
      // Callback to discover items.
      // Should return either scalar array of entity ids or
      // array of array, with each element containing 'id' key with value +
      // additional data.
      'discovery callback' => function () {
        return [
          [
            'id' => 1,
            'custom1' => 'some custom data',
          ],
          [
            'id' => 2,
            'custom2' => 'other custom data',
          ],
        ];
      },
      'process callback' => function ($entity, $data) {
        $node = $entity;
        // Change a field value.
        $node->title .= ' changed';
        return BulkProcessItemProcessor::ENTITY_NEEDS_SAVE;
      },
    ],
  ];
}
