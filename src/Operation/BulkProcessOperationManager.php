<?php

namespace Drupal\bulk_process\Operation;

class BulkProcessOperationManager {

  protected static $operations;

  /**
   * @return \Drupal\bulk_process\Operation\BulkProcessOperation []
   * @throws \Exception
   */
  public function getOperations() {
    if (!isset(self::$operations)) {
      self::$operations = [];

      $info = \Drupal::moduleHandler()->invokeAll('bulk_process_operations_info');

      foreach ($info as $operation_info) {
        $operation = BulkProcessOperation::fromArray($operation_info);
        self::$operations[$operation->getName()] = $operation;
      }
    }

    return self::$operations;
  }

  public function getOperationByName($name) {
    $operations = $this->getOperations();
    foreach ($operations as $operation) {
      if ($operation->getName() == $name) {
        return $operation;
      }
    }

    return NULL;
  }
}
