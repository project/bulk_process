<?php

namespace Drupal\bulk_process\Operation;

class BulkProcessOperation {

  protected $name;

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * @param mixed $entityType
   */
  public function setEntityType($entityType) {
    $this->entityType = $entityType;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getDiscoveryCallback() {
    return $this->discoveryCallback;
  }

  /**
   * @param mixed $callback
   */
  public function setDiscoveryCallback($callback) {
    $this->validateCallback($callback);
    $this->discoveryCallback = $callback;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getProcessCallback() {
    return $this->processCallback;
  }

  /**
   * @param mixed $processCallback
   */
  public function setProcessCallback($callback) {
    $this->validateCallback($callback);
    $this->processCallback = $callback;

    return $this;
  }

  protected $entityType;

  protected $discoveryCallback;

  protected $processCallback;

  public function __construct($name, $entity_type, $discovery_callback, $process_callback) {
    $this
      ->setName($name)
      ->setEntityType($entity_type)
      ->setDiscoveryCallback($discovery_callback)
      ->setProcessCallback($process_callback);
  }

  protected function validateCallback($callback) {
    if (!is_callable($callback)) {
      throw new \Exception(sprintf('Operation callback "%s" is not callable.', is_array($callback) ? implode('::', $callback) : $callback));
    }
  }

  public static function fromArray($values) {
    $required_keys = [
      'name',
      'entity_type',
      'discovery callback',
      'process callback',
    ];

    $missing_keys = array_flip(array_diff_key(array_flip($required_keys), $values));
    if (count($missing_keys) > 0) {
      throw new \Exception(sprintf('Missing key(s) "%s" in operation definition.', implode('", "', $missing_keys)));
    }

    $empty_values = array_keys(array_diff_key($values, array_filter($values)));
    if (count($empty_values) > 0) {
      throw new \Exception(sprintf('Found empty items with keys "%s" in operation definition.', implode('", "', array_keys($empty_values))));
    }

    return new self($values['name'], $values['entity_type'], $values['discovery callback'], $values['process callback']);
  }

}
