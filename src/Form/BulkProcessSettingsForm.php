<?php

namespace Drupal\bulk_process\Form;

use Drupal\bulk_process\BulkProcessProcessManager;
use Drupal\bulk_process\Operation\BulkProcessOperationManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BulkProcessSettingsForm.
 */
class BulkProcessSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The main processor.
   *
   * @var \Drupal\bulk_process\BulkProcessProcessManager
   */
  protected $processManager;

  protected $operationManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, BulkProcessProcessManager $process_manager, BulkProcessOperationManager $operation_manager) {
    $this->setConfigFactory($config_factory);
    $this->processManager = $process_manager;
    $this->operationManager = $operation_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('bulk_process.process_manager'),
      $container->get('bulk_process.operation_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bulk_process_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bulk_process.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bulk_process.settings');

    $operations_options = $this->getOperationsOptions();
    if (empty($operations_options)) {
      return [
        '#markup' => $this->t('No operations available. Please implement <code>hook_bulk_process_operations_info()</code> and refresh this page.'),
      ];
    }

    $form['info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Processing information'),
    ];
    $active_operations = $config->get('active_operations');
    $form['info']['active_operations'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Operations'),
      '#description' => $this->t('Select which operations should participate in discovery and processing. Note that already queued items will be processed using previously active operations.'),
      '#options' => $operations_options,
      '#default_value' => !empty($active_operations) ? $active_operations : [],
    ];
    $form['info']['discovered'] = [
      '#markup' => $this->t('<strong>Total discovered:</strong> @count', [
        '@count' => $this->processManager->getDiscoveredCount(),
      ]),
      '#suffix' => '<br/>',
    ];
    $form['info']['queued'] = [
      '#markup' => $this->t('<strong>Total queued:</strong> @count', [
        '@count' => $this->processManager->getQueuedCount(),
      ]),
      '#suffix' => '<br/>',
    ];
    $form['info']['processed'] = [
      '#markup' => $this->t('<strong>Total processed:</strong> @count ', [
        '@count' => $this->processManager->getProcessedCount(),
      ]),
      '#suffix' => '<br/>',
    ];

    $form['info']['log_link'] = [
      '#markup' => Link::fromTextAndUrl($this->t('View log'), Url::fromUserInput('/admin/config/development/bulk_process/log'))->toString(),
    ];
    $form['info']['actions'] = [
      '#type' => 'actions',
    ];
    $form['info']['actions']['reset_queue_and_rediscover'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset queue and re-discover items'),
      '#description' => $this->t('Remove currently discovered items from the queue, rediscover the items and add them to the queue.'),
    ];

    $form['manual'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Manual run'),
    ];
    $form['manual']['manual_run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Process now'),
      '#prefix' => '<div class="container-inline">',
    ];
    $form['manual']['manual_qty'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 1000,
      '#step' => 1,
      '#default_value' => '100',
      '#suffix' => $this->t('items') . '</div>',
    ];

    $form['single'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Single item processing'),
    ];
    $form['single']['single_operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#options' => $operations_options,
    ];
    $form['single']['single_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#options' => $this->getEntityTypesOptions(),
      '#default_value' => 'node',
      '#prefix' => '<div class="container-inline">',
    ];
    $form['single']['single_entity_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Entity ID'),
      '#min' => 1,
      '#max' => 10000000,
      '#suffix' => '</div>',
    ];
    $form['single']['single_data'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Data'),
      '#description' => $this->t('Custom data to pass to the single item processor. One record per line. Use pipe <code>|</code> symbol to separate keys and values.'),
      '#rows' => 3,
      '#cols' => 80,
    ];
    $form['single']['actions'] = [
      '#type' => 'actions',
    ];
    $form['single']['actions']['process_single'] = [
      '#type' => 'submit',
      '#value' => $this->t('Process single'),
    ];

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
    ];
    $form['settings']['batch_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Batch size'),
      '#description' => $this->t('Number of items to process within a single batch run.'),
      '#min' => 1,
      '#max' => 1000,
      '#step' => 1,
      '#default_value' => $config->get('batch_size'),
    ];
    $form['settings']['use_log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use log'),
      '#description' => $this->t('Log processing of items.'),
      '#default_value' => $config->get('use_log'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('bulk_process.settings_form'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $op = (string) $form_state->getValue('op');
    if ($op == (string) $this->t('Save')) {
      $config = $this->config('bulk_process.settings');

      $config->set('batch_size', $form_state->getValue('batch_size'));
      $config->set('use_log', $form_state->getValue('use_log'));
      $config->set('active_operations', array_filter($form_state->getValue('active_operations')));
      $config->save();

      parent::submitForm($form, $form_state);
    }
    elseif ($op == (string) $this->t('Reset queue and re-discover items')) {
      $this->submitFormResetQueueAndDiscover($form, $form_state);
    }
    elseif ($op == (string) $this->t('Process now')) {
      $this->submitFormProcessManual($form, $form_state);
    }
    elseif ($op == (string) $this->t('Process single')) {
      $this->submitFormProcessSingle($form, $form_state);
    }
  }

  protected function getOperationsOptions() {
    $operations = $this->operationManager->getOperations();

    return array_combine(array_keys($operations), array_keys($operations));
  }

  protected function getEntityTypesOptions() {
    $options = [];

    $info = \Drupal::entityTypeManager()->getDefinitions();

    foreach ($info as $name => $entity_type) {
      $options[$name] = $entity_type->getLabel();
    }

    return $options;
  }

  protected function submitFormProcessManual(array &$form, FormStateInterface $form_state) {
    $qty = (int) $form_state->getValue('manual_qty');

    $processed_qty = $this->processManager->process($qty);

    $this->messenger()->addStatus($this->formatPlural($qty, 'Manually processed 1 item', 'Manually processed @count items', [
      '@count' => $processed_qty,
    ]));
  }

  protected function submitFormResetQueueAndDiscover(array &$form, FormStateInterface $form_state) {
    $operations_names = array_filter($form_state->getValue('active_operations'));
    $this->processManager->discover($operations_names);

    $this->messenger()->addStatus($this->t('The queue was reset and items were re-discovered'));
  }

  protected function submitFormProcessSingle(array &$form, FormStateInterface $form_state) {
    $entity_type = $form_state->getValue('single_entity_type');
    $entity_id = (int) $form_state->getValue('single_entity_id');
    $operation_name = $form_state->getValue('single_operation');
    $data = $form_state->getValue('single_data');
    $data = $this->parseCustomData($data);

    $operation = $this->operationManager->getOperationByName($operation_name);
    if (!$operation) {
      throw new \Exception($this->t('Unexpected operation name @name provided', [
        '@operation_name' => $operation_name,
      ]));
    }

    $this->processManager->processSingle($entity_type, $entity_id, $operation, $data);

    $this->messenger()->addStatus($this->t('Single %entity_type item with id %id has been processed using operation %operation. @link_view or @link_edit.', [
      '%entity_type' => $entity_type,
      '%id' => $entity_id,
      '%operation' => $operation->getName(),
      '@link_view' => Link::createFromRoute($this->t('View'), 'entity.' . $entity_type . '.canonical', [$entity_type => $entity_id])->toString(),
      '@link_edit' => Link::createFromRoute($this->t('Edit'), 'entity.' . $entity_type . '.edit_form', [$entity_type => $entity_id])->toString(),
    ]));
  }

  protected function parseCustomData($data) {
    $array = [];

    $lines = preg_split('~\R~', $data);

    foreach ($lines as $line) {
      if (strpos($line, '|') !== FALSE) {
        list($k, $v) = explode('|', $line);
        $array[trim($k)] = trim($v);
      }
      else {
        $array[] = trim($line);
      }
    }

    $array = array_filter($array);

    return $array;
  }

}
