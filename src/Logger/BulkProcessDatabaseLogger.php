<?php

namespace Drupal\bulk_process\Logger;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Database;

/**
 * Class BulkProcessDatabaseLogger.
 *
 * @package Drupal\bulk_process\Logger
 */
class BulkProcessDatabaseLogger implements BulkProcessLoggerInterface {

  /**
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('bulk_process.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function log($event, $message, $severity = self::SEVERITY_INFO) {
    if (!$this->isEnabled()) {
      return;
    }

    $connection = Database::getConnection();
    $connection->insert('bulk_process_log')
      ->fields([
        'event',
        'severity',
        'timestamp',
        'message',
      ])
      ->values([
        'event' => $event,
        'severity' => $severity,
        'timestamp' => microtime(TRUE),
        'message' => $message,
      ])
      ->execute();
  }

  protected function isEnabled() {
    return $this->config->get('use_log');
  }

  /**
   * Severity map.
   */
  protected static function severityMap($key = NULL) {
    $map = [
      self::SEVERITY_EMERGENCY => 'Emergency',
      self::SEVERITY_ALERT => 'Alert',
      self::SEVERITY_CRITICAL => 'Critical',
      self::SEVERITY_ERROR => 'Error',
      self::SEVERITY_WARNING => 'Warning',
      self::SEVERITY_NOTICE => 'Notice',
      self::SEVERITY_INFO => 'Info',
    ];

    return isset($map[$key]) ? $map[$key] : $map;
  }

  /**
   * {@inheritdoc}
   */
  public function reset() {
    $connection = Database::getConnection();
    $connection->truncate('bulk_process_log')->execute();
  }

}
