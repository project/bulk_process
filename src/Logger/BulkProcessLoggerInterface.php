<?php

namespace Drupal\bulk_process\Logger;

interface BulkProcessLoggerInterface {

  const SEVERITY_EMERGENCY = 0;

  const SEVERITY_ALERT = 1;

  const SEVERITY_CRITICAL = 2;

  const SEVERITY_ERROR = 3;

  const SEVERITY_WARNING = 4;

  const SEVERITY_NOTICE = 5;

  const SEVERITY_INFO = 6;

  public function log($event, $message, $severity = self::SEVERITY_INFO);

  public function reset();

}
