<?php

namespace Drupal\bulk_process\Exception;

/**
 * Class BulkProcessItemProcessException.
 *
 * Thrown when item processing has failed. Not supposed to stop further
 * processing.
 *
 * @package Drupal\bulk_process\Exception
 */
class BulkProcessItemProcessException extends \Exception {

}
