<?php

namespace Drupal\bulk_process\Counter;

class BulkProcessProcessedCounter extends BulkProcessCounterBase {

  protected function getName() {
    return 'bulk_process.counter.processed';
  }

}
