<?php

namespace Drupal\bulk_process\Counter;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class BulkProcessCounterBase implements ContainerInjectionInterface {

  protected $count;

  protected $stateStorage;

  public function __construct(StateInterface $state_storage) {
    $this->stateStorage = $state_storage;
    $this->load();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  abstract protected function getName();

  public function load() {
    $this->count = $this->stateStorage->get($this->getName(), 0);

    return $this;
  }

  public function save() {
    $this->stateStorage->set($this->getName(), $this->count);

    return $this;
  }

  public function getCount() {
    return $this->count;
  }

  public function increment() {
    $this->count++;

    return $this;
  }

  public function reset() {
    $this->count = 0;

    return $this;
  }

}
