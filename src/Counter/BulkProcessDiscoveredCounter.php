<?php

namespace Drupal\bulk_process\Counter;

class BulkProcessDiscoveredCounter extends BulkProcessCounterBase {

  protected function getName() {
    return 'bulk_process.counter.discovered';
  }

}
