<?php

namespace Drupal\bulk_process\Plugin\QueueWorker;

use Drupal\bulk_process\Counter\BulkProcessProcessedCounter;
use Drupal\bulk_process\Exception\BulkProcessItemProcessException;
use Drupal\bulk_process\ItemProcessor\BulkProcessItemProcessor;
use Drupal\bulk_process\Logger\BulkProcessLoggerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class BulkProcessQueueBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\bulk_process\ItemProcessor\BulkProcessItemProcessor
   */
  protected $itemProcessor;

  /**
   * @var BulkProcessProcessedCounter
   */
  protected $processedCounter;

  /**
   * @var \Drupal\bulk_process\Logger\BulkProcessLoggerInterface
   */
  protected $logger;

  /**
   * BulkProcessQueueBase constructor.
   *
   * @param \Drupal\bulk_process\ItemProcessor\BulkProcessItemProcessorBase $item_processor
   *   The item processor.
   * @param \Drupal\bulk_process\Logger\BulkProcessLoggerInterface $logger
   *   The logger.
   */
  public function __construct(BulkProcessItemProcessor $item_processor, BulkProcessProcessedCounter $processed_counter, BulkProcessLoggerInterface $logger) {
    $this->itemProcessor = $item_processor;
    $this->processedCounter = $processed_counter;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    // @hack! Need to create item_processor factory and inject here to
    // allow processing of entities of different types.
      $container->get('bulk_process.item_processor'),
      $container->get('bulk_process.counter.processed'),
      $container->get('bulk_process.logger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    try {
      $this->logger->log('item_process', print_r($data, TRUE));

      if (empty($data['entity_type'])) {
        throw new \Exception(sprintf('Required key "entity_type" is missing in provided data. %s', print_r($data, TRUE)));
      }

      $this->itemProcessor->process($data);

      $this->processedCounter->increment()->save();
    }
    catch (BulkProcessItemProcessException $exception) {
      // Do not re-throw exception to avoid stale (always failing) items in the
      // queue. Just log the problem.
      $this->logger->log('item_process_failed', $exception->getMessage());
    }
    catch (\Exception $exception) {
      $this->logger->log('item_process_suspended', $exception->getMessage());
      // Suspending the queue if unexpected exception was thrown. All entity
      // exceptions should be handled by ItemProcessor. Anything else is
      // a critical error.
      throw new SuspendQueueException($exception->getMessage());
    }
  }

}
