<?php

namespace Drupal\bulk_process\Plugin\QueueWorker;

/**
 * A Bulk Processor that processes items on CRON run.
 *
 * @QueueWorker(
 *   id = "bulk_process_processor_cron",
 *   title = @Translation("Cron Bulk Processor"),
 *   cron = {"time" = 60}
 * )
 */
class BulkProcessCronQueue extends BulkProcessQueueBase {

}
