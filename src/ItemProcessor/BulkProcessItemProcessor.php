<?php

namespace Drupal\bulk_process\ItemProcessor;

use Drupal\bulk_process\Exception\BulkProcessItemProcessException;
use Drupal\bulk_process\Operation\BulkProcessOperationManager;
use Drupal\Core\Entity\EntityBase;

class BulkProcessItemProcessor {

  const ENTITY_UNCHANGED = 0;

  const ENTITY_NEEDS_SAVE = 1;

  const ENTITY_NEEDS_RELOAD = 2;

  protected $operationManager;


  public function __construct(BulkProcessOperationManager $operation_manager) {
    $this->operationManager = $operation_manager;
  }

  public function process($data) {
    $this->validateData($data);

    $entity_type = $data['entity_type'];
    $entity_id = $data['entity_id'];

    /** @var \Drupal\Core\Entity\EntityStorageInterface $entity_storage */
    $entity_storage = \Drupal::entityTypeManager()->getStorage($entity_type);

    $entity = $entity_storage->load($entity_id);

    if (!$entity) {
      throw new BulkProcessItemProcessException(sprintf('Unable to load "%s" entity with id "%s".', $entity_type, $entity_id));
    }

    $entity_should_be_saved = FALSE;

    // Run processors.
    foreach ($data['operations'] as $operation_name => $operation_data) {
      // @todo: Add try/catch?
      $operation = $this->operationManager->getOperationByName($operation_name);

      // Allow to bypass saving changes to the entity itself if child entities
      // where saved.
      // If return is FALSE - treat is as explicit unchanged entity.
      // If return is TRUE - treat is as explicit changed entity.
      // If return is NULL - treat is as explicit changed entity.
      $return = call_user_func($operation->getProcessCallback(), $entity, $operation_data);

      // Treat non-returned value as changed entity.
      $return = is_null($return) ? self::ENTITY_NEEDS_SAVE : $return;

      // Validate returned values.
      if (!in_array($return, [self::ENTITY_UNCHANGED, self::ENTITY_NEEDS_SAVE, self::ENTITY_NEEDS_RELOAD])) {
        throw new \Exception(sprintf('Unexpected value was returned from hook_bulk_process_operations_info() implementation.'));
      }

      // Reload entity if explicitly asked for it.
      if ($return === self::ENTITY_NEEDS_RELOAD) {
        $entity = $this->reloadEntity($entity);
      }
      // Or schedule save is asked (will apply to all even if only one operation
      // process asked for explicit save).
      elseif ($return === self::ENTITY_NEEDS_SAVE) {
        $entity_should_be_saved = TRUE;
      }
    }

    // Save only if asked for it. The counter case would be operations where
    // resulting entity object would be unchanged since entity's referenced
    // items perform saving themselves within processing operations (i.e.,
    // paragraphs call $para->entity->save()).
    if ($entity_should_be_saved) {
      $entity->save();
    }
  }

  protected function validateData($data) {
    $required_keys = [
      'entity_id',
      'entity_type',
      'operations',
    ];

    $missing_keys = array_flip(array_diff_key(array_flip($required_keys), $data));
    if (count($missing_keys) > 0) {
      throw new \Exception(sprintf('Missing key(s) "%s" in process data definition.', implode('", "', $missing_keys)));
    }

    $empty_values = array_keys(array_diff_key($data, array_filter($data)));
    if (count($empty_values) > 0) {
      throw new \Exception(sprintf('Found empty items with keys "%s" in data definition.', implode('", "', array_keys($empty_values))));
    }
  }

  protected function entitiesAreEqual($entity1, $entity2) {
    // @todo: Implement this.
    return FALSE;
  }

  /**
   * Forcefully reload entity.
   */
  protected function reloadEntity(EntityBase $entity) {
    return entity_load($entity->getEntityTypeId(), $entity->id(), TRUE);
  }

}

