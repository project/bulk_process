<?php

namespace Drupal\bulk_process;

use Drupal\bulk_process\Counter\BulkProcessDiscoveredCounter;
use Drupal\bulk_process\Counter\BulkProcessProcessedCounter;
use Drupal\bulk_process\ItemProcessor\BulkProcessItemProcessor;
use Drupal\bulk_process\Logger\BulkProcessLoggerInterface;
use Drupal\bulk_process\Operation\BulkProcessOperation;
use Drupal\bulk_process\Operation\BulkProcessOperationManager;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;

/**
 * Class BulkProcessProcessManager.
 *
 * @package Drupal\bulk_process
 */
class BulkProcessProcessManager {

  /**
   * @var \Drupal\bulk_process\ItemProcessor\BulkProcessItemProcessor
   */
  protected $itemProcessor;

  /**
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * @var BulkProcessLoggerInterface
   */
  protected $logger;

  /**
   * @var
   */
  protected $discoveredCounter;

  protected $processedCounter;


  protected $operationManager;


  /**
   * Constructor.
   */
  public function __construct(
    BulkProcessItemProcessor $item_processor,
    BulkProcessOperationManager $operation_manager,
    QueueFactory $queue_factory,
    QueueWorkerManagerInterface $queue_manager,
    BulkProcessDiscoveredCounter $discovered_counter,
    BulkProcessProcessedCounter $processed_counter,
    BulkProcessLoggerInterface $logger
  ) {
    $this->itemProcessor = $item_processor;
    $this->operationManager = $operation_manager;
    $this->queueFactory = $queue_factory;
    $this->queueManager = $queue_manager;
    $this->discoveredCounter = $discovered_counter;
    $this->processedCounter = $processed_counter;
    $this->logger = $logger;
  }

  /**
   * Discover new items to put in the queue.
   */
  public function discover(array $operations_names = []) {
    $this->logger->reset();

    $items = $this->doDiscover($operations_names);

    $this->discoveredCounter->reset();
    $this->processedCounter->reset()->save();

    $queue = $this->getQueue();
    $queue->deleteQueue();

    foreach ($items as $item) {
      $queue->createItem($item);
      $this->discoveredCounter->increment();
    }

    $this->discoveredCounter->save();

    return $this->discoveredCounter->getCount();
  }

  /**
   * Discover items and create flat array to store meta information.
   *
   * @throws \Exception
   */
  protected function doDiscover(array $operations_names = []) {
    $discovered = [];

    $operations = $this->operationManager->getOperations();

    foreach ($operations as $operation) {
      // Skip operation if it has not been explicitly specified.
      if (!empty($operations_names) && !in_array($operation->getName(), $operations_names)) {
        continue;
      }

      // Call discovery callback to get all discovered data. Note that each
      // operation supports only one entity type.
      // Returned values can be either an array of integer ids or
      // an array of arrays with 'id' key and id value + other custom data.
      // @todo: Add try/catch?
      $data = call_user_func($operation->getDiscoveryCallback());

      // Callback may not return any values.
      $data = empty($data) ? [] : $data;

      foreach ($data as $item) {
        $item = is_array($item) ? $item : ['id' => $item];

        if (empty($item['id'])) {
          continue;
        }

        // Extract ID and remove it from the item to then use it as custom data.
        $id = $item['id'];
        unset($item['id']);

        // Using per-content type and ID key to uniquely identify entities.
        $key = $operation->getEntityType() . '_' . $id;
        $discovered[$key]['entity_id'] = $id;
        $discovered[$key]['entity_type'] = $operation->getEntityType();
        // Collect all operations for this ID with custom data.
        $discovered[$key]['operations'][$operation->getName()] = $item;
      }
    }

    // Do not return our internal keys.
    $discovered = array_values($discovered);

    return $discovered;
  }

  /**
   * Manually process number of items from the queue.
   *
   * @param $qty
   */
  public function process($qty) {
    $queue = $this->getQueue();
    $queue_worker = $this->getQueueWorker();

    $qty = min($queue->numberOfItems(), $qty);

    $this->logger->log('manual_process_started', sprintf('Manual process started for %s item(s)', $qty), BulkProcessLoggerInterface::SEVERITY_INFO);

    $count = 0;
    while ($count < $qty && ($item = $queue->claimItem())) {
      try {
        $queue_worker->processItem($item->data);
        $queue->deleteItem($item);
        $count++;
        $this->logger->log('manual_process_success', sprintf('Manual process succeeded after processing an item with data: %s', print_r($item->data, TRUE)), BulkProcessLoggerInterface::SEVERITY_INFO);
      }
      catch (\Exception $exception) {
        // Remove the item from the queue to avoid stale items that never free
        // up the queue.
        $queue->deleteItem($item);
        // @todo: Review severity level.
        $this->logger->log('manual_process_skip', sprintf('Manual process skipped with error: %s', $exception->getMessage()), BulkProcessLoggerInterface::SEVERITY_WARNING);
      }
    }

    $this->logger->log('manual_process_finished', sprintf('Manual process finished for %s item(s)', $qty), BulkProcessLoggerInterface::SEVERITY_INFO);

    return $qty;
  }

  public function processSingle($entity_type, $entity_id, BulkProcessOperation $operation, $data = NULL) {
    $item_data = [
      'entity_id' => $entity_id,
      'entity_type' => $entity_type,
      'operations' => [$operation->getName() => $data],
    ];
    $this->itemProcessor->process($item_data);
  }

  public function getDiscoveredCount() {
    return $this->discoveredCounter->getCount();
  }

  public function getQueuedCount() {
    return $this->getQueue()->numberOfItems();
  }

  public function getProcessedCount() {
    return $this->processedCounter->getCount();
  }

  /**
   * @return \Drupal\Core\Queue\QueueInterface
   */
  protected function getQueue() {
    return $this->queueFactory->get('bulk_process_processor_cron');
  }

  protected function getQueueWorker() {
    return $this->queueManager->createInstance('bulk_process_processor_cron');
  }


}
